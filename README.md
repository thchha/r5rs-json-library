# Opinionated JSON Library For R5RS+

This should be a R5RS portable json library.

It reads and writes to a port.
Ports do not get closed after use.

Your implementation may not define `(error`
In this case you should provide an own implementation.

## elaboration

Every json-object is an alist.
An json-array is a vector.

It is in use within my software and even tested;
Tests will be added, once a testing framework was picked.

# LICENSE

MPLv2, but I just do not want to grant warranty on this.
Feel free to contact me about a license change.
