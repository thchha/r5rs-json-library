;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  Copyright © 2022 - by thchha / Thomas Hage
;;
;; This Source Code Form is subject to the terms of the Mozilla Public
;; License, v. 2.0. If a copy of the MPL was not distributed with this
;; file, You can obtain one at https://mozilla.org/MPL/2.0/.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Curried access to assoc
(define-syntax json-assoc
  (syntax-rules ()
    ((_ obj key)
     (assoc key obj))
    ((_ obj key rest ...)
     (if (assoc key obj)
         (json-assoc (cdr (assoc key obj)) rest ...)
         #f))))

(define (json-decode port)
  (letrec ((hex->int (lambda (c) (string->number (string c) 16)))
           (decode-string
             (lambda ()
               (let add ((str '()))
                 (let ((c (read-char port)))
                 (case c
                   ((#\") (list->string (reverse str)))
                   ((#\\)
                    (case (read-char port)
                      ((#\") (add (cons #\" str)))
                      ((#\\) (add (cons #\\ str)))
                      ((#\/) (add (cons #\/ str)))
                      ((#\b) (add (cons #\backspace str)))
                      ((#\f) (add (cons #\page str)))
                      ((#\n) (add (cons #\newline str)))
                      ((#\r) (add (cons #\return str)))
                      ((#\t) (add (cons #\tab str)))
                      ((#\u) (add (cons (integer->char
                                           (+ (* (hex->int (read-char port)) (expt 16 0))
                                              (* (hex->int (read-char port)) (expt 16 1))
                                              (* (hex->int (read-char port)) (expt 16 2))
                                              (* (hex->int (read-char port)) (expt 16 3))))
                                         str)))))
                   (else (add (cons c str))))))))

           (decode-number
             (lambda ()
               (let loop ((num '()))
                 (case (peek-char port)
                   ((#\0 #\1 #\2 #\3 #\4 #\5 #\6 #\7 #\8 #\9 #\- #\. #\e #\+ #\E)
                    (loop (cons (read-char port) num)))
                   (else (string->number (list->string (reverse num))))))))

           (decode-object
             (lambda (json)
               (case (read-char port)
                 ((#\space #\linefeed #\return #\tab) (decode-object json))
                 ((#\,)                               (decode-object json))
                 ((#\") (let lp ((str (decode-string)))
                          (case (read-char port)
                            ((#\space #\linefeed #\return #\tab) (lp str))
                            ((#\:) (decode-object (cons (cons str (decode-value json)) json)))
                            (else (error "value in json-object expected")))))
                 ((#\}) (reverse json))
                 (else (error "key in json-object expected")))))

           (decode-array
             (lambda (arr)
               (case (peek-char port)
                 ((#\space #\linefeed #\return #\tab)
                  (read-char port)
                  (decode-array arr))
                 ((#\])
                  (read-char port)
                  (list->vector (reverse arr)))
                 (else
                   (let ((val (decode-value '())))
                     (case (read-char port)
                       ((#\space #\linefeed #\return #\tab)
                              (decode-array (cons val arr)))
                       ((#\,) (decode-array (cons val arr)))
                       ((#\]) (list->vector (reverse (cons val arr))))
                       (else (error "unexpected object in json-array"))))))))

           (decode-value
             (lambda (json)
               (let ((c (peek-char port)))
                 (case c
                   ((#\space #\linefeed #\return #\tab)
                    (read-char port) ;; drop
                    (decode-value json))
                   ((#\")
                    (read-char port)
                    (decode-string))
                   ((#\- #\0 #\1 #\2 #\3 #\4 #\5 #\6 #\7 #\8 #\9) (decode-number))
                   ((#\{)
                    (read-char port)
                    (decode-object '()))
                   ((#\[)
                    (read-char port)
                    (decode-array '()))
                   ((#\t) (decode-true))
                   ((#\f) (decode-false))
                   ((#\n) (decode-null))
                   (else (error (string-append
                                  "invalid json provided. Unexpected char: "
                                  (object->string c))))))))

           (decode-true
             (lambda ()
               (let ((_1 (read-char port))
                     (_2 (read-char port))
                     (_3 (read-char port))
                     (_4 (read-char port)))
                 (if (and (char=? _1 #\t)
                          (char=? _2 #\r)
                          (char=? _3 #\u)
                          (char=? _4 #\e))
                     #t
                     (error "invalid true-value provided")))))

           (decode-false
             (lambda ()
               (let ((_1 (read-char port))
                     (_2 (read-char port))
                     (_3 (read-char port))
                     (_4 (read-char port))
                     (_5 (read-char port)))
                 (if (and (char=? _1 #\f)
                          (char=? _2 #\a)
                          (char=? _3 #\l)
                          (char=? _4 #\s)
                          (char=? _5 #\e))
                     #f
                     (error "invalid false-value provided")))))

           (decode-null
             (lambda ()
               (let ((_1 (read-char port))
                     (_2 (read-char port))
                     (_3 (read-char port))
                     (_4 (read-char port)))
                 (if (and (char=? _1 #\n)
                          (char=? _2 #\u)
                          (char=? _3 #\l)
                          (char=? _4 #\l))
                     '()
                     (error "invalid null-value provided"))))))
    (if (eof-object? (peek-char port))
        (error "provided port is already closed.")
        (decode-value '()))))

(define (json-encode json port)
  (letrec ((start-walk
             (lambda (json)
               (cond ((pair? json)
                      (display #\{ port) (walk json) (display #\} port))
                     (else (walk json)))))
           (walk
             (lambda (lst)
               (if (not (pair? lst))
                   (cond ((vector? lst) (display #\[ port)
                                        (let lp ((items (vector->list lst)))
                                          (when (not (null? items))
                                            (start-walk (car items))
                                            (if (not (null? (cdr items)))
                                                (display #\, port))
                                            (lp (cdr items))))
                                        (display #\] port))
                         ((not lst)     (display "false" port))
                         ((null? lst)   (display "null"  port))
                         ((eqv? #t lst) (display "true"  port))
                         (else          (write lst port)))
                   (let ((key (car lst))
                         (value (cdr lst)))
                     (cond
                       ((pair? key)
                        (walk key)
                        (when
                          (and (list? value)
                               (not (null? value)))
                          (display #\, port)
                          (walk value)))
                       ((not (pair? key))
                        (walk key)
                        (display #\: port)
                        (start-walk value))
                       (else
                         (walk key)
                         (walk value))))))))
    (start-walk json)))
